<?php
require "../models/Caso.php";

$caso = new Caso();

switch ($_GET['op']) {
    case 'listar':

        $rsta = $caso->listar();

        $data = array();

        foreach ($rsta as $rta) {
            //if ($_SESSION['Id_analista'] == 'administrador') {
            $data[] = array('0' => ($rta->condicion) ? '<button class="btn btn-warning" onclick="mostrar(' . $rta->idcaso . ')"><i class="fa fa-pencil"></i></button>' :
                '<button class="btn btn-warning" onclick="mostrar(' . $rta->idcaso . ')"><i class="fa fa-pencil"></i></button>' .
                ' <button class="btn btn-primary" onclick="activar(' . $rta->idcaso . ')"><i class="    fa fa-check"></i></button>',

                '1'                 => $rta->nombre_analista,
                '2'                 => $rta->fecha_hora,
                '3'                 => $rta->nombre_caso,
                '4'                 => $rta->npasos, 
                '5'                 => $rta->complejidad,
                '6'                 => $rta->rediseno,
                '7'                 => $rta->id_diseno,
                '8'                 => ($rta->perfilado) ? '<button class="btn btn-warning" disabled><span>si</span></button> ' : '<button class="btn btn-success" disabled ><span>no</span></button>',
                '9'                 => ($rta->ejecucion) ? '<button class="btn btn-warning" disabled><span>si</span></button> ' : '<button class="btn btn-success" disabled ><span>no</span></button>',

            );

            // }

        }

        $result = array('sEcho' => 1, 'iTotalRecords' => count($data), 'iTotalDisplayRecords' => count($data), 'aaData' => $data);
        echo json_encode($result);

        break;

    case 'insertaryeditar':
        $id = isset($_POST['idcaso']) == 0 ? "" : $_POST['idcaso'];
        session_start();
        $caso->setId_Analista($_SESSION['Id_analista']);
       
        $caso->setNombre_Caso($_POST['nombre_caso']);
        $caso->setNpasos($_POST['npasos']);
        $caso->setComplejidad($_POST['complejidad']);
        $caso->setRediseno($_POST['rediseno']);

        $caso->setId_Diseno($_POST['id_diseno']);
        $caso->setPerfilado($_POST['perfilado']);
        $caso->setEjecucion($_POST['ejecucion']);

        if ($id) {
            $caso->setIdcaso($id);
            $rta = $caso->Modificar($caso);
            echo $rta ? "se edito correctamente" : "No se pudo editar";
        } else {

            $rta = $caso->Insertar($caso);
            echo $rta ? "se inserto correctamente" : "No se pudo insertar";
        }

        break;

    case 'mostrar':

        $id  = $_POST['idcaso'];
        $rta = $caso->Obtener($id);
        echo json_encode($rta);
        break;

    case 'desactivar':

        $id  = $_POST['idcaso'];
        $rta = $caso->Eliminar($id);
        echo $rta ? "Se desactivo la caso" : "no se ha podido desactivar";
        break;

    case 'activar':

        $id  = $_POST['idcaso'];
        $rta = $caso->activar($id);
        echo $rta ? "se activo la caso" : "no se ha podido activar";
        break;

    default:
        echo "Opcion no permitida";
        break;
}
