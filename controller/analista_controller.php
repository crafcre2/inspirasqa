<?php

require_once "../models/Analista.php";

$analista = new Analista();

switch ($_GET['op']) {
    case 'salir':
        session_start();
        //limpiaar sesiones
        session_unset();
        //se destruye la sesion
        session_destroy();

        header('location: ../index.php');
        break;

    case 'Ingresar':
        $usuario = isset($_POST['logina']) ? $_POST['logina'] : "";
        $contra  = isset($_POST['clavea']) ? $_POST['clavea'] : "";

        $pass = md5($contra);
        $rta  = $analista->Validar($usuario, $pass);

        if (!empty($rta)) {
            session_start();
            $_SESSION['Id_analista'] = $rta->Id_analista;
            $_SESSION['rol']         = $rta->rol;
            $_SESSION['nombre']      = $rta->nombre;

        } else {
            $rta = null;

        }
        //echo $_SESSION["rol"];
        echo json_encode($rta);

        break;
    case 'listar':

        $rsta = $analista->listar();

        $data = array();

        foreach ($rsta as $rta) {
            $data[] = array('0' => ($rta->condicion) ? '<button class="btn btn-warning" onclick="mostrar(' . $rta->Id_analista . ')"><i class="fa fa-pencil"></i></button>' .
                ' <button class="btn btn-danger" onclick="desactivar(' . $rta->Id_analista . ')"><i class="fa fa-close"></i></button>' :
                '<button class="btn btn-warning" onclick="mostrar(' . $rta->Id_analista . ')"><i class="fa fa-pencil"></i></button>' .
                ' <button class="btn btn-primary" onclick="activar(' . $rta->Id_analista . ')"><i class="    fa fa-check"></i></button>',

                '1'                 => $rta->nombre,
                '2'                 => $rta->usuario,

                '3'                 => $rta->celula,
                '4'                 => $rta->rol,

            );

        }

        $result = array('sEcho' => 1, 'iTotalRecords' => count($data), 'iTotalDisplayRecords' => count($data), 'aaData' => $data);
        echo json_encode($result);

        break;

    case 'insertaryeditar':
        $id = isset($_POST['id_analista']) ? $_POST['id_analista'] : "";
        $analista->setNombre($_POST['nombre']);
        $analista->setUsuario($_POST['usuario']);

        $analista->setPass(md5($_POST['pass']));
        $analista->setCelula($_POST['celula']);
        $analista->setRol($_POST['rol']);

        if ($id) {
            $analista->setId_Analista($id);
            $rta = $analista->Modificar($analista);
            echo $rta ? "se edito correctamente" : "No se pudo editar";
        } else {

            $rta = $analista->Insertar($analista);
            echo $rta ? "se inserto correctamente" : "No se pudo insertar";
        }

        break;

    case 'cargarcelula':

        $rsta = $analista->cargacelula();

        foreach ($rsta as $reg) {
            echo "<option value=" . $reg->Id_celula . ">" . $reg->nombre . "</option>";

        }

        break;

    case 'mostrar':

        $id  = $_POST['id_analista'];
        $rta = $analista->Obtener($id);
        echo json_encode($rta);
        break;

    case 'desactivar':

        $id  = $_POST['id_analista'];
        $rta = $analista->Eliminar($id);
        echo $rta ? "Se desactivo la analista" : "no se ha podido desactivar";
        break;

    case 'activar':

        $id  = $_POST['id_analista'];
        $rta = $analista->activar($id);
        echo $rta ? "se activo la analista" : "no se ha podido activar";
        break;

    default:
        echo "# code...";
        break;
}
