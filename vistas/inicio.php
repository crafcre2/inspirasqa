<?php
//activar almacenamiento del buffer
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location : ../Index.php");

} else {

    $a = 1;
    require 'header.php';

    ?>
      <div id="lstregistro" class="center-block col-md-12">
         <h1>Casos</h1>


          <table id="tbllistado" class="table table-hover" >
                        <thead>
                          <th>Editar</th>
                          <th>Nom. Analista</th>
                          <th>Fecha</th>
                          <th>Nom. Caso</th>
                          <th>Num. Pasos</th>
                          <th>Complejidad</th>
                          <th>Rediseño</th>
                          <th>Nom. Diseño</th>
                           <th>Perfilado</th>
                           <th>Ejecucion</th>



                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <th>Editar</th>
                          <th>Nom. Analista</th>
                          <th>Fecha</th>
                          <th>Nom. Caso</th>
                          <th>Num. Pasos</th>
                          <th>Complejidad</th>
                          <th>Rediseño</th>
                          <th>Nom. Diseño</th>
                          <th>Perfilado</th>
                          <th>Ejecucion</th>
                        </tfoot>
                      </table>

                </div>
                <div>
                  <button id="mostrarform" onclick="mostrarformulario(true)" type="button" class="btn btn-success">Agregar</button>

                </div>

             <div class="row justify-content-center  col-md-12"  id="formRegistro">
              <div><h1> Registro Casos</h1></div>

               <form class="row justify-content-center col-md-12" name="formulario" id="formulario" onsubmit="return false" >




                    


                              <div class="form-group-label col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Numero Pasos:</label>
                            <input type="number" class="form-control" name="npasos" id="npasos">


                            </div>

                                  <div class="form-group-label col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Id Caso:</label>
                            <input type="text" class="form-control" name="id_diseno" id="id_diseno" >


                            </div>




                              <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Complejidad</label>
                                    <select id="complejidad" name="complejidad" class="form-control four-boot" data-live-search="true">
                                       <option value="baja">Baja</option>
                                      <option value="media">Media</option>
                                       <option value="alta">Alta</option>

                                  </select>
                                </div>

                                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Rediseño</label>
                                    <select id="rediseno" name="rediseno" class="form-control four-boot" data-live-search="true" required>
                                       <option value="no">No</option>
                                      <option value="si ">Si</option>


                                  </select>

                                  </div>

                                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Perfilado</label>
                                    <select id="perfilado" name="perfilado" class="form-control four-boot" data-live-search="true" required>
                                       <option value="0">No</option>
                                      <option value="1">Si</option>


                                  </select>

                                  </div>

                                  <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <label>Ejecucion</label>
                                    <select id="ejecucion" name="ejecucion" class="form-control four-boot" data-live-search="true" required>
                                       <option value="0">No</option>
                                      <option value="1">Si</option>


                                  </select>

                                  </div>



                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <strong style="color: #fff;" class=" form-control-label">Nom. Caso </strong>

                                    <input type="descripcion" name="nombre_caso" id="nombre_caso"  maxlength="256" class="form-control">


                                    <label  class="form-text">ex. UP_TV HBO + Internet ...</label>

                                  </div>










                   <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <button type="submit" onclick="guardaryeditar()" class="btn btn-primary"  id="btnguardar"><i class="fa fa-save" ></i>Guardar</button>
                   <button  onclick="cancelar();"class="btn btn-danger"  id="btncalcelar"><i class="fa fa-arrow-circle-left" ></i>Cancelar</button>

                </div>
              </form>


                </div>






<?php

    require 'footer.php';
    ?>
<script type="text/javascript" src="js/caso.js"></script>

</html>
<?php
}
//liberar espacio buffer
ob_end_flush();

?>
