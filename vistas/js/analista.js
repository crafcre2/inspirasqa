$(document).ready(function() {
    mostrarformulario(false);
    listar();
    //carga select
    $.post("../controller/analista_controller.php?op=cargarcelula", function(r) {
        $("#celula").html(r);
    });
});

function mostrarformulario(flag) {
    if (flag) {
        $('#lstregistro').hide();
        $('#formRegistro').show();
        $('#mostrarform').hide();
    } else {
        $('#lstregistro').show();
        $('#formRegistro').hide();
        $('#mostrarform').show();
    }
}

function listar() {
    //bootstrap-data-table-export
    tabla = $('#tbllistado').dataTable({
        "aProcessing": true, //activamos el proceso dde datatables
        "aServerSide": true,
        "dom": 'Bfrtip',
        buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdf'],
        "ajax": {
            url: '../controller/analista_controller.php?op=listar',
            type: "get",
            dataType: "json",
            error: function(e) {
                console.log(e.responseText);
            }
        },
        "bDestroiy": true,
        "iDisplayLength": 10,
        "order": [
            [0, "desc"]
        ]
    }).DataTable();
}

function cancelar() {
    limpiarCapos();
    mostrarformulario(false);
}

function limpiarCapos() {
    $('#nombre').val = "";
    $('#usuario').val = "";
    $('#pass').val = "";
    $("#celula").val = "";
    $("#rol").val("");
}

function guardaryeditar() {
    //e.preventDefault();
    $('#btnguardar').prop("disable", true);
    //entender mejor esta linea
    var formData = new FormData(document.getElementById("formulario"));
    console.log(formData);
    $.ajax({
        url: "../controller/analista_controller.php?op=insertaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function(datos) {
            mostrarformulario(false);
            tabla.ajax.reload();
            bootbox.alert(datos);
        }
    }).fail(function(ex) {
        console.log(ex);
        //window.location.reload();
    });
    limpiarCapos();
}

function mostrar(id_analista) {
    $.post("../controller/analista_controller.php?op=mostrar", {
        id_analista: id_analista
    }, function(data, status) {
        data = JSON.parse(data);
        console.log(data);
        mostrarformulario(true);
        $('#id_analista').val(data.Id_analista);
        $('#nombre').val(data.nombre);
        $('#usuario').val(data.usuario);
        $('#pass').val(data.pass);
        $("#celula").val(data.celula);
        $("#rol").val(data.rol);
    });
}

function desactivar(id) {
    var a = confirm("¿Esta seguro de querer Eliminar el caso?");
    if (a == true) {
        $.post("../controller/analista_controller.php?op=desactivar", {
            id_analista: id
        }, function(e) {
            bootbox.alert(e);
            tabla.ajax.reload();
        });
    }
}

function activar(id) {
    var a = confirm("¿Esta seguro de querer activar el caso?");
    if (a == true) {
        $.post("../controller/analista_controller.php?op=activar", {
            id_analista: id
        }, function(e) {
            bootbox.alert(e);
            tabla.ajax.reload();
        });
    }
}
$('#cierre').on("click", function() {
    bootbox.alert("cerrar");
    $.get("analista_controller.php?op=", function(data, status) {
        location.href = "login.html";
    });
});