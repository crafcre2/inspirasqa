$(document).ready(function() {
    mostrarformulario(false);
    listar();
    $(".date").datepicker({
        dateFormat: 'yy-mm-dd'
    });
    //se cargan los items de checkbox permisos
});

function mostrarformulario(flag) {
    if (flag) {
        $('#lstregistro').hide();
        $('#formRegistro').show();
        $('#mostrarform').hide();
    } else {
        $('#lstregistro').show();
        $('#formRegistro').hide();
        $('#mostrarform').show();
    }
}

function listar() {
    //bootstrap-data-table-export
    tabla = $('#tbllistado').dataTable({
        "aProcessing": true, //activamos el proceso dde datatables
        "aServerSide": true,
        "dom": 'Bfrtip',
        buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdf'],
        "ajax": {
            url: '../controller/caso_controller.php?op=listar',
            type: "get",
            dataType: "json",
            error: function(e) {
                console.log(e.responseText);
            }
        },
        "bDestroiy": true,
        "iDisplayLength": 5,
        "order": [
            [0, "desc"]
        ]
    }).DataTable();
}

function cancelar() {
    limpiarCapos();
    mostrarformulario(false);
}

function limpiarCapos() {
    $('#fecha_hora').val = "";
    $('#npasos').val = "";
    $('#complejidad').val = "";
    $("#rediseno").val = "";
    $("#nombre_caso").val("");
    $("#id_diseno").val("");
}

function guardaryeditar() {
    //e.preventDefault();
    $('#btnguardar').prop("disable", true);
    //entender mejor esta linea
    var formData = new FormData(document.getElementById("formulario"));
    console.log(formData);
    $.ajax({
        url: "../controller/caso_controller.php?op=insertaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        success: function(datos) {
            mostrarformulario(false);
            tabla.ajax.reload();
            bootbox.alert(datos);
        }
    }).fail(function(ex) {
        bootbox.alert(ex);
        //window.location.reload();
    });
    limpiarCapos();
}

function mostrar(idcaso) {
    $.post("../controller/caso_controller.php?op=mostrar", {
        idcaso: idcaso
    }, function(data, status) {
        data = JSON.parse(data);
        console.log(data);
        mostrarformulario(true);
        $('#idcaso').val(data.Idcaso);
       
        $('#npasos').val(data.npasos);
        $('#complejidad').val(data.complejidad);
        $("#rediseno").val(data.rediseno ? "si" : "no");
        $("#nombre_caso").val(data.id_diseno);
        $("#id_diseno").val(data.nombre_caso);
    });
}

function desactivar(idcaso) {
    var a = confirm("¿Esta seguro de querer Eliminar el caso?");
    if (a == true) {
        $.post("../controller/caso_controller.php?op=desactivar", {
            idcaso: idcaso
        }, function(e) {
            bootbox.alert(e);
            tabla.ajax.reload();
        });
    }
}

function activar(id) {
    var a = confirm("¿Esta seguro de querer activar el caso?");
    if (a == true) {
        $.post("../controller/caso_controller.php?op=activar", {
            idcaso: id
        }, function(e) {
            bootbox.alert(e);
            tabla.ajax.reload();
        });
    }
}
document.getElementById("npasos").addEventListener("change", cambiaComplejidad);
function cambiaComplejidad() {
	    var valor = document.getElementById("npasos").value;
	    if (valor < 21) {
		            document.getElementById("complejidad").value = 'baja';
		        } else if (valor > 20 && valor < 71) {
				        document.getElementById("complejidad").value = 'media';
				    } else if (valor > 70) {
					            document.getElementById("complejidad").value = 'alta';
					        } else {
							        document.getElementById("complejidad").value = '';
							    }
}
