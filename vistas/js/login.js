$("#entrar").on('click', function(e) {
    e.preventDefault();
    var logina = document.getElementById('logina').value;
    var clavea = document.getElementById('clavea').value;
    console.log(logina + " " + clavea);
    $.post("../controller/analista_controller.php?op=Ingresar", {
        "logina": logina,
        "clavea": clavea
    }, function(data) {
        var array = data;
        if (data == "null") {
            alert("Usuario y/o Password incorrectos");
        } else {
            location.href = 'inicio.php';
        }
    });
})