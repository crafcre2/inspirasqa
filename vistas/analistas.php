<?php
//activar almacenamiento del buffer
ob_start();
session_start();
$a = 1;
if (!isset($_SESSION['Id_analista'])) {
    header("Location : login.html");
} else {
    require 'header.php';
    if ($_SESSION['rol'] == 'administrador') {

        ?>



      <div id="lstregistro" class="center-block col-md-12">

        <h1> Analistas</h1>

          <table id="tbllistado" class="table table-hover">
                        <thead>
                          <th>Editar\Eliminar</th>
                          <th>Nombre</th>
                          <th>Usuario</th>
                          <th>Celula</th>
                          <th>Rol</th>




                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <th>Editar\Eliminar</th>
                          <th>Nombre</th>
                          <th>Usuario</th>
                          <th>Celula</th>
                          <th>Rol</th>
                        </tfoot>
                      </table>

                </div>
                <div>
                  <button id="mostrarform" onclick="mostrarformulario(true)" type="button" class="btn btn-success">Agregar</button>

                </div>


                <div id="formRegistro" class="row justify-content-center">

                       <div style="padding-left: 480px" class="col-md-12"> <h1>Registro Analistas</h1></div>

               <form  class="row justify-content-center col-md-6"  name="formulario" id="formulario" onsubmit="return false" >







                              <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Nombre:</label>
                            <input type="text" class="form-control" name="nombre" id="nombre">
                             <input type="hidden" class="form-control" name="id_analista" id="id_analista">


                            </div>



                              <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Usuario:</label>
                            <input type="text" class="form-control" name="usuario" id="usuario">


                            </div>


                                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label>Contraseña:</label>
                            <input type="password" class="form-control" name="pass" id="pass">


                            </div>

                                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <label>Celula</label>
                  <select id="celula" name="celula" class="form-control" data-live-search="true" required>

                </select>

                </div>


                                      <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <label>Rol</label>
                                    <select id="rol" name="rol" class="form-control four-boot" data-live-search="true" required>
                                       <option value="administrador">Administrador</option>
                                      <option value="tester">Testater</option>


                                  </select>


                            </div>
                                      <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <button  onclick="guardaryeditar()" class="btn btn-primary"  id="btnguardar"><i class="fa fa-save" ></i>Guardar</button>
                   <button  onclick="cancelar();"class="btn btn-danger"  id="btncalcelar"><i class="fa fa-arrow-circle-left" ></i>Cancelar</button>

                </div>

                    </div>


              </form>



        </div>

 <?php
} else {
        require 'noacceso.php';

    }
    require "footer.php";

    ?>
<script type="text/javascript" src="js/analista.js"></script>
<?php
}
//liberar espacio buffer
ob_end_flush();

?>