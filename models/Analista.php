<?php
require "Conexion.php";

class Analista extends Conexion
{
    public $pdo;

    public $idanalista;
    public $nombre;
    public $usuario;
    public $contrasena;
    public $celula;
    public $condicion;
    public $rol;

//encapsulamiento
    //-----------------------------------

    public function __CONSTRUCT()
    {
        $this->pdo = Conexion::Conectar();
    }

    public function getId_Analista()
    {
        return $this->idanalista;
    }

    public function setId_Analista($id)
    {
        $this->idanalista = $id;
    }
    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre($id)
    {
        $this->nombre = $id;
    }
    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setUsuario($id)
    {
        $this->usuario = $id;
    }
    public function getPass()
    {
        return $this->pass;
    }

    public function setPass($id)
    {
        $this->pass = $id;
    }
    public function getCelula()
    {
        return $this->celula;
    }

    public function setCelula($id)
    {
        $this->celula = $id;
    }
    public function getRol()
    {
        return $this->rol;
    }

    public function setRol($id)
    {
        $this->rol = $id;
    }
    public function getCondicion()
    {
        return $this->condicion;
    }

    public function setCondicion($id)
    {
        $this->condicion = $id;
    }

//---------------------------------------------------

    public function Listar()
    {

        try {
            $consulta  = "SELECT a.Id_analista as Id_analista , a.nombre as nombre, a.usuario as usuario, c.nombre as celula, rol, condicion FROM analista a INNER JOIN celula c ON a.Id_celula = c.Id_celula";
            $sentencia = $this->pdo->prepare($consulta);
            $sentencia->execute();

            return $sentencia->fetchALL(PDO::FETCH_OBJ);

        } catch (exception $e) {
            die($e->getMessage());
        }
    }

    public function Validar($user, $pwd)
    {
        try {

            $consulta = "SELECT a.Id_analista as Id_analista , a.nombre as nombre, a.usuario as usuario, c.nombre as celula, rol, condicion FROM analista a INNER JOIN celula c ON a.Id_celula = c.Id_celula WHERE a.usuario='$user' AND a.pass='$pwd'";

            $rta = $this->pdo->prepare($consulta);
            $rta->execute();
            return $rta->fetch(PDO::FETCH_OBJ);

        } catch (Exception $e) {
            die($e->getMessage());
            return null;
        }

    }

    public function Obtener($id)
    {
        try {
            $consulta  = "SELECT a.Id_analista as Id_analista , a.nombre as nombre, a.usuario as usuario, a.pass as pass, c.Id_celula as celula, rol, condicion FROM analista a INNER JOIN celula c ON a.Id_celula = c.Id_celula  where a.Id_analista = '$id'";
            $sentencia = $this->pdo->prepare($consulta);
            $sentencia->execute();

            return $sentencia->fetch(PDO::FETCH_OBJ);

        } catch (exception $e) {
            die($e->getMessage());
        }
    }

    public function Insertar(Analista $a)
    {
        try {
            $consulta = "INSERT INTO `analista` (`nombre`,`usuario`, `pass`, `Id_celula`,`rol`) VALUES (?,?,?,?,?);";

            $this->pdo->prepare($consulta)->execute(
                array(
                    $a->getNombre(),
                    $a->getUsuario(),
                    $a->getPass(),
                    $a->getCelula(),
                    $a->getRol(),

                ));
            return true;

        } catch (exception $e) {
            die($e->getMessage());
        }
    }

    public function Modificar(Analista $a)
    {
        try {
            $consulta = " UPDATE analista SET
                nombre = ?,
                usuario = ?,
                pass = ?,
                Id_celula = ?,
                rol = ?

                WHERE Id_analista = ?;";

            $this->pdo->prepare($consulta)->execute(
                array(
                    $a->getNombre(),
                    $a->getUsuario(),
                    $a->getPass(),
                    $a->getCelula(),
                    $a->getRol(),

                    $a->getId_Analista(),
                ));
            return true;

        } catch (exception $e) {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        $sql = "UPDATE analista SET condicion = '0' WHERE Id_analista = '$id'";
        $rta = $this->pdo->prepare($sql);
        $rta->execute();

        return "Se elimino usuario";

    }

    public function cargacelula()
    {
        try {

            $sql = "SELECT * FROM celula";
            $rta = $this->pdo->prepare($sql);
            $rta->execute();
            return $rta->fetchALL(PDO::FETCH_OBJ);

        } catch (exception $e) {
            die($e->getMessage());

        }

    }

    //Activar una usuario
    public function activar($id)
    {
        $sql = "UPDATE analista SET condicion='1' WHERE Id_analista='$id'";
        $rta = $this->pdo->prepare($sql);
        $rta->execute();
        return "Activacion Correcta";
    }

}
