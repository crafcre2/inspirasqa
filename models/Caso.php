<?php
require "Conexion.php";
class Caso extends Conexion
{
    private $pdo;
    private $idcaso;
    private $id_analista;
    private $fecha_hora;
    private $nombre_caso;
    private $npasos;
    private $complejidad;
    private $rediseno;
    private $condicion;
    private $id_diseno;
    private $perfilado;
    private $ejecucion;

//encapsulamiento
    //-----------------------------------

    public function __CONSTRUCT()
    {
        $this->pdo = Conexion::Conectar();
    }

    public function getIdcaso()
    {
        return $this->idcaso;
    }

    public function setIdcaso($id)
    {
        $this->idcaso = $id;
    }

    public function getEjecucion()
    {
        return $this->ejecucion;
    }

    public function setEjecucion($id)
    {
        $this->ejecucion = $id;
    }

    public function getPerfilado()
    {
        return $this->perfilado;
    }

    public function setPerfilado($id)
    {
        $this->perfilado = $id;
    }

    public function getId_Analista()
    {
        return $this->id_analista;
    }

    public function setId_Analista($id)
    {
        $this->id_analista = $id;
    }

    public function getFecha_Hora()
    {
        return $this->fecha_hora;
    }

    public function setFecha_Hora($id)
    {
        $this->fecha_hora = $id;
    }public function getNombre_Caso()
    {
        return $this->nombre_caso;
    }

    public function setNombre_Caso($id)
    {
        $this->nombre_caso = $id;
    }

    public function getNpasos()
    {
        return $this->npasos;
    }

    public function setNpasos($id)
    {
        $this->npasos = $id;
    }

    public function getComplejidad()
    {
        return $this->complejidad;
    }

    public function setComplejidad($id)
    {
        $this->complejidad = $id;
    }

    public function getRediseno()
    {
        return $this->rediseno;
    }

    public function setRediseno($id)
    {
        $this->rediseno = $id;
    }

    public function getCondicion()
    {
        return $this->condicion;
    }

    public function setCondicion($id)
    {
        $this->condicion = $id;
    }
    public function getId_Diseno()
    {
        return $this->id_diseno;
    }

    public function setId_Diseno($id)
    {
        $this->id_diseno = $id;
    }

//---------------------------------------------------

    public function Listar()
    {

        try {
            $consulta = "SELECT c.Idcaso as idcaso, a.nombre as nombre_analista, c.nombre_caso as nombre_caso, c.fecha_hora as fecha_hora, c.npasos as npasos, c.complejidad as complejidad, c.rediseno as rediseno, c.condicion as condicion, c.id_diseno as id_diseno, c.perfilado as perfilado, c.ejecucion as ejecucion FROM caso c inner join analista a
on c.id_analista = a.Id_analista order by c.fecha_hora DESC";
            $sentencia = $this->pdo->prepare($consulta);
            $sentencia->execute();

            return $sentencia->fetchALL(PDO::FETCH_OBJ);

        } catch (exception $e) {
            die($e->getMessage());

        }
    }

    public function Obtener($id)
    {
        try {
            $consulta  = "SELECT * FROM caso where Idcaso = '$id'";
            $sentencia = $this->pdo->prepare($consulta);
            $sentencia->execute();

            return $sentencia->fetch(PDO::FETCH_OBJ);

        } catch (exception $e) {
            die($e->getMessage());
        }
    }

    public function Insertar(Caso $c)
    {
        try {
            $consulta = "INSERT INTO caso (id_analista,fecha_hora, nombre_caso,     npasos,complejidad,rediseno,condicion,  id_diseno, perfilado, ejecucion) VALUES (?,now(),?,?,?,?,1,?,?,?);";

            $this->pdo->prepare($consulta)->execute(
                array(
                    $c->getId_Analista(),
                   
                    $c->getId_Diseno(),
                    $c->getNpasos(),
                    $c->getComplejidad(),
                    $c->getRediseno(),
                    $c->getNombre_Caso(),
                    $c->getPerfilado(),
                    $c->getEjecucion(),
                ));

            return true;

        } catch (exception $e) {
            die($e->getMessage());
        }
    }

    public function Modificar(Caso $c)
    {
        try {
            $consulta = " UPDATE caso SET
                fecha_hora = ?,
                nombre_caso = ?,
                npasos = ?,
                complejidad = ?,
                rediseno = ?,
                id_diseno = ?,
                perfilado = ?,
                ejecucion = ?
                WHERE idcaso = ?;";

            $this->pdo->prepare($consulta)->execute(
                array(

                    $c->getFecha_Hora(),
                    $c->getId_Diseno(),

                    $c->getNpasos(),
                    $c->getComplejidad(),
                    $c->getRediseno(),

                    $c->getNombre_Caso(),
                    $c->getPerfilado(),
                    $c->getEjecucion(),
                    $c->getIdcaso(),
                ));

            return true;

        } catch (exception $e) {
            die($e->getMessage());
        }
    }

    public function Eliminar($id)
    {
        $sql = "UPDATE caso SET condicion = '0' WHERE Idcaso = '$id'";
        $rta = $this->pdo->prepare($sql);
        $rta->execute();

        return "Se elimino usuario";

    }

    //Activar una caso
    public function activar($idcaso)
    {
        $sql = "UPDATE caso SET condicion='1' WHERE Idcaso='$idcaso'";
        $rta = $this->pdo->prepare($sql);
        $rta->execute();
        return "Activacion Correcta";
    }

}
